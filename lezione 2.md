Lezione 3:

Facciamo una prova pratica su un sito di film famosi.

Se usate Visual Studio Code, in basso a sinistra c'è una x,
se ci cliccate sopra vi appare la finestra con delle tabs, 
fra cui c'è anche TERMINAL che vi permette di usare una shell dall'editor.
Entriamo nella cartella Example ed editiamo il nostro file index.js:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {

    const response = await request(URL);

    console.log(response);
    
})()
```

Come potete vedere stiamo cercando di puntare un indirizzo web, 
e dopo l'attesa della risposta lo visualizziamo nella console del browser.

Ricordiamo che dal browser per visualizzare pagine web noi facciamo delle richieste a cui il sito risponde con del codice.
Questo codice che ci ritorna è poi interpretato e ci permette di vedere la pagina con il contenuto,
ma possiamo sempre visualizzarne il sorgente premendo shift+ctrl+i (su chrome e firefox) 

Per fare funzionare il nostro script, dalla riga di comando, dentro Example, diamo:

```
npm install --save request-promise
npm install --save cheerio
```

questo per installare localmente le librerie richiamate dal file index.js.

Ora se usiamo visual studio code andiamo dal menu su 'debug' ed avviamo lo script dal triangolino verde.
Vedremo in basso, dalla 'debug console' il risultato.

Altrimenti ad ogni modo, dalla cartella Example, diamo:
node index.js

Avviando quindi index.js avremo nella shell il codice della pagina richiesta, 
che è uguale a quello che leggiamo dalla console del browser se premiamo shift+ctrl+i.

Ora, se modifichiamo il codice:

```
const request = require('request-promise');
const cheerio = require('cheerio');

const URL='https://www.imdb.com/title/tt0102926/?ref_=nv_sr_1';

(async () => {

    const response = await request(URL);
    
    let $=cheerio.load(response);
    let title=$('div[class="title_wrapper"] > h1').text();//dell'elemento div riferito alla classe title_wrapper, prendiamo il child h1
    console.log(title);
  
})()
```

avremo intercettato la risposta del sito e potremo cosi' gestire il DOM e navigarci dentro per acquisire i dati che ci servono.

Nel nostro caso stampiamo sulla nostra shell il child h1 dell'elemento div di classe "title_wrapper":
Il silenzio degli innocenti (1991)

> The Silence of the Lambs (1991)
> Directed by Jonathan Demme.  With Jodie Foster, Anthony Hopkins, Lawrence A. Bonney, Kasi Lemmons. A young FBI cadet > must receive the help of an incarcerated and manipulative cannibal killer to help catch another serial killer, a > > > madman who skins his victims.