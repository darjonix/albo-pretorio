Lezione 1:

Prima di andare avanti comprendiamo un concetto importantissimo in Javascript, il DOM.

Il DOM (Document Object Model) è un modello, ovvero un modo di rappresentare un documento. 
Se pensiamo al contenuto HTML di una pagina web il DOM è un'interfaccia,
o meglio una API (Application Programming Interface) ideata dal consorzio W3C, 
che permette di accedere agli elementi di una pagina.

Prendiamo questa pagina html:

```
<html>
<body>
<div id=”immagini”><img src=”pippo.jpg”></div>
</body>
</html>
```

se guardiamo all'HTML dal punto di vista strutturale, diremo che la tag img è figlia della tag div, che a sua volta è figlia della tag body e così via. 
In altre parole la catena di relazioni “padre-figlio” che individuano la tag img è

```
html > body > div > img
```

Se però vogliamo accedere al contenuto della tag img usando il DOM, potremo scrivere

```
<script language="javascript" type="text/javascript">
alert(document.images[0].src) ;
</script>
```

che ci mostrerà un messaggio del tipo fotografia.jpg
La cosa importante da osservare è che tramite il DOM abbiamo avuto accesso alla tag img direttamente, 
senza attraversare la catena gerarchica delle sequenza padre-figlio. 

Il DOM ci permette dunque di trovare senza troppa fatica gli elementi che stiamo cercando all'interno della pagina. 

Ad esempio, se vogliamo stampare la lista di tutte le immagini presenti nel codice HTML basterà scrivere

```
for (i = 0; i < document.images.length; i++) {
document.write(document.images[i].src + "<br/>");
}
```

Un ultima cosa importante:
Il DOM è dinamico. E' possibile cambiare o aggiungere elementi al DOM, modificando la pagina dopo averla visualizzata nel browser!